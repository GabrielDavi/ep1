#include <iostream>
#include <string>
#include "../inc/produto.hpp"

using namespace std;

Produto::Produto()
{
}

Produto::~Produto()
{
}

void Produto::setNome(string nome)
{
    this->nome = nome;
}

string Produto::getNome()
{
    return nome;
}

void Produto::setCategoria(string categoria)
{
    this->categoria = categoria;
}

string Produto::getcategoria()
{
    return categoria;
}

void Produto::setPreco(int preco)
{
    this->preco = preco;
}

int Produto::getPreco()
{
    return preco;
}