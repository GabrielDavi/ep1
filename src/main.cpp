#include <iostream>
#include "../inc/cliente.hpp"
#include "../inc/produto.hpp"

using namespace std;

int main()
{

    Cliente cliente1;

    Produto produto1;

    string name, cpf, email, productName, categoria;
    int idade, preco;

    cout << "insira o nome: " << endl;
    cin >> name;
    cliente1.setNome(name);
    cout << "insira a idade: " << endl;
    cin >> idade;
    cliente1.setIdade(idade);
    cout << "insira a email: " << endl;
    cin >> email;
    cliente1.setEmail(email);
    cout << "insira a Cpf: " << endl;
    cin >> cpf;
    cliente1.setCpf(cpf);
    cout << "insira nome do produto: " << endl;
    cin >> productName;
    produto1.setNome(productName);
    cout << "insira preço do produto: " << endl;
    cin >> preco;
    produto1.setPreco(preco);
    cout << "insira categoria do produto: " << endl;
    cin >> categoria;
    produto1.setCategoria(categoria);

    cout << "nome: " << cliente1.getNome() << endl;
    cout << "idade: " << cliente1.getIdade() << endl;
    cout << "email: " << cliente1.getEmail() << endl;
    cout << "cpf: " << cliente1.getCpf() << endl;
    cout << "nome do produto: " << produto1.getNome() << endl;
    cout << "preço do produto: " << produto1.getPreco() << endl;
    cout << "categoria do produto: " << produto1.getcategoria() << endl;

    return 0;
}