#include <iostream>
#include <string>
#include "../inc/cliente.hpp"

using namespace std;

Cliente::Cliente(){
   
}

Cliente::~Cliente(){

}

void Cliente::setNome(string nome)
{
    this->nome = nome;
}

string Cliente::getNome(){
    return nome;
}

void Cliente::setIdade(int idade)
{
    this->idade = idade;
}

int Cliente::getIdade(){
    return idade;
}

void Cliente::setCpf(string cpf){
    this->cpf = cpf;
}

string Cliente::getCpf(){
    return cpf;
}

void Cliente::setEmail(string email){
    this->email = email;
}

string Cliente::getEmail(){
    return email;
}

