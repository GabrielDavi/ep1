#ifndef _CLIENTE_
#define _CLIENTE_

#include <string>

class Cliente
{

private:
    std::string nome, cpf, email;
    int idade;

public:
    Cliente();
    ~Cliente();

    void setNome(std::string nome);
    std::string getNome();
    void setCpf(std::string cpf);
    std::string getCpf();
    void setEmail(std::string email);
    std::string getEmail();
    void setIdade(int idade);
    int getIdade();

    void registraDados();
};

#endif