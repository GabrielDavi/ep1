#ifndef _PRODUTO_
#define _PRODUTO_

#include <string>

class Produto
{
private:
    std::string nome, categoria;
    int preco;

public:
    Produto();
    ~Produto();

    void setNome(std::string nome);
    std::string getNome();
    void setCategoria(std::string categoria);
    std::string getcategoria();
    void setPreco(int preco);
    int getPreco();

    void registraDados();
};

#endif